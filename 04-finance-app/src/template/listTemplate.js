export class ListTemplate {
  constructor(listContainer) {
    this.listContainer = listContainer;
  }

  render(doc, resultId) {
    const liElement = document.createElement("li");
    liElement.innerHTML = doc.format();
    liElement.setAttribute("id", resultId);
    liElement.classList.add("list-group-item", "p-3");
    this.listContainer.appendChild(liElement);
  }
}

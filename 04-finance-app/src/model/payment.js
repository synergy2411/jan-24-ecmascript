export class PaymentModel {
  #baseUrl;

  constructor() {
    this.#baseUrl = "http://localhost:3000";
  }

  async createPayment(payment) {
    try {
      const response = await fetch(`${this.#baseUrl}/payments`, {
        method: "POST",
        body: JSON.stringify(payment),
        headers: {
          "Content-Type": "application/json",
        },
      });

      const result = await response.json();

      return result;
    } catch (err) {
      console.error(err);
    }
  }
  async getPayments() {
    try {
      const response = await fetch(`${this.#baseUrl}/payments`);
      const allPayments = await response.json();
      return allPayments;
    } catch (err) {
      console.error(err);
    }
  }

  deletePayment(paymentId) {
    // Should Delete the item from Database and UI
  }
}

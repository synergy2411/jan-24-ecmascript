export class PaymentType {
  constructor(title, amount, createdAt) {
    this.title = title;
    this.amount = amount;
    const year = createdAt.getFullYear();
    const day = createdAt.toLocaleString("en-US", { day: "numeric" });
    const month = createdAt.toLocaleString("en-US", { month: "long" });
    this.createdAt = `${month} ${day}, ${year}`;
  }
  format() {
    return `${this.title} of amount INR ${this.amount} dated ${this.createdAt}`;
  }
}

import { PaymentType } from "./paymentType";

export class Invoice extends PaymentType {
  constructor(title, amount, createdAt) {
    super(title, amount, createdAt);
  }

  format() {
    return `Invoice made for ${super.format()}`;
  }
}

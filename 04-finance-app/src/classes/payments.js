import { PaymentType } from "./paymentType";

export class Payment extends PaymentType {
  constructor(title, amount, createdAt) {
    super(title, amount, createdAt);
  }

  format() {
    return `Payment received for ${super.format()}`;
  }
}

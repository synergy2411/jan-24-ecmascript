import { Invoice } from "./classes/invoice";
import { Payment } from "./classes/payments";
import { PaymentModel } from "./model/payment";
import { ListTemplate } from "./template/listTemplate";

window.onload = function () {
  const paymentTypeEl = document.getElementById("paymentType");
  const titleEl = document.getElementById("title");
  const amountEl = document.getElementById("amount");
  const createdAtEl = document.getElementById("createdAt");
  const btnAdd = document.getElementById("btnAdd");
  const btnReset = document.getElementById("btnReset");
  const listContainer = document.getElementById("listContainer");

  let listTemplate = new ListTemplate(listContainer);
  let model = new PaymentModel();

  function loadPayments() {
    model.getPayments().then((allPayments) => {
      console.log("ALL PAYMENTS : ", allPayments);
      allPayments.forEach((payment) => {
        let doc;
        let title = payment.title;
        let amount = Number(payment.amount);
        let createdAt = new Date(payment.createdAt);
        if (payment.paymentType === "invoice") {
          doc = new Invoice(title, amount, createdAt);
        } else {
          doc = new Payment(title, amount, createdAt);
        }
        listTemplate.render(doc);
      });
    });
  }

  loadPayments();

  btnAdd.addEventListener("click", async function (event) {
    event.preventDefault();

    let title = titleEl.value;
    let amount = Number(amountEl.value);
    let createdAt = new Date(createdAtEl.value);
    let paymentType = paymentTypeEl.value;

    let doc;

    if (paymentType === "invoice") {
      doc = new Invoice(title, amount, createdAt);
    } else {
      doc = new Payment(title, amount, createdAt);
    }

    let payment = {
      ...doc,
      paymentType,
    };
    try {
      const result = await model.createPayment(payment);
      listTemplate.render(doc, result.id);
    } catch (err) {
      console.error(err);
    }
  });

  btnReset.addEventListener("click", function (e) {
    e.preventDefault();
    titleEl.value = "";
    amountEl.value = "";
    createdAtEl.value = "";
  });
};

const rnd = Math.round(Math.random() * 100);

const randomNumber = () => rnd;

const getDailyQuotes = () => "Run 5 miles today!";

// Named Export
export { randomNumber };

// Default Export
export default getDailyQuotes;

import { sum, mul } from "./utils/maths";
import myQuotes, { randomNumber } from "./utils/fortune";

// import myQuotes from "./utils/fortune";      // Default Value

import "./css/styles.css";

console.log("LOADED!!!!");

console.log("Sum : ", sum(2, 4));

console.log("Multiply : ", mul(2, 5));

console.log("Your lucky number today is : ", randomNumber());

console.log("Daily Quotes : ", myQuotes());

const btnLoad = document.getElementById("btnLoad");

btnLoad.addEventListener("click", function (e) {
  e.preventDefault();
  import("./app.js").then((m) => m.log());
});

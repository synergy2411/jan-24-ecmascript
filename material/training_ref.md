# Break Timing

- Tea : 11:00AM (15 mins)
- Lunch : 1:00PM (45 mins)
- Tea : 3:30PM (15 mins)

# JavaScript

- Dynamic Language - Types determined at runtime by JS Engine
- Asynchronous - Micro / Macro
- DOM Manipulation -
- Scripting Language - AST -> Interpreted by browser / NRE
- Client as well as Server Side
- OOPS Concepts - ES2015 / ES6
- Single Threaded - JS app runs on single thread
- Non-blocking - Single thread does not block
- Loosely Typed - Types determined at runtime
- Promises : placeholders for future values

# Async Behaviour

- Micro : Promises, queueMicrotask()
- Macro : Timer API

# Full Stack App

- MEAN : MongoDB, Express, Angular, NodeJS
- MERN : MongoDB, Express, React, NodeJS

- JSON Document

# JavaScript Engine

- Creational Phase : Memory is allocated
- Executional Phase : code execution

---

- DOM API
- Arrow functions
- Destructuring
- Spread / Rest
- Classes
- Default Parameter
- Map / Set
- Promise
  > then().catch()
  > Async...await
  > Static Methods (resolve, reject, race, any, all, allSettled)
- fetch API
- Finance App

---

01 - Generate package.json

> npm init -y

02 - Webpack Dependencies

> npm install webpack webpack-cli webpack-dev-server -D

03 - CSS Loaders

> npm install style-loader css-loader -D

04 - Babel libraries and loader

> npm install @babel/core @babel/preset-env babel-loader -D

npm run build

# Module Types

- ESM Modules - default export / import
- CommonJS Modules - default Node (require / module.exports)
- AMD
- UMD

---

# Generators : function whose execution is not continuous; returns the iterator object

---

MVP :
MVVM : 2 way data binding; ViewModel- VM (knockoutJS); Observer API
MVC : Angular

[] - Property
() - EventBinding

[(ngModel)]= ""

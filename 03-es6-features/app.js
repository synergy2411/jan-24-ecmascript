// window.onload = function () {
//   const btnFetch = document.querySelector("#btnFetch");
//   const listContainer = document.querySelector("#listContainer");
//   btnFetch.addEventListener("click", function () {
//     let xhr = new XMLHttpRequest();

//     xhr.onload = function () {
//       if (xhr.readyState === 4 && xhr.status === 200) {
//         const todos = JSON.parse(xhr.response);
//         todos.forEach((todo) => {
//           const liElement = document.createElement("li");
//           liElement.innerHTML = todo.title;
//           listContainer.appendChild(liElement);
//         });
//       }
//     };

//     xhr.open("GET", "https://jsonplaceholder.typicode.com/todos");

//     xhr.send();
//   });
// };

// PROMISES

// producer : new Promise();

// function promiseProducer(arr = []) {
//   const promise = new Promise((resolve, reject) => {
//     if (arr.length > 2) {
//       setTimeout(() => resolve({ message: "SUCCESS" }), 1000);
//     } else {
//       setTimeout(() => reject(new Error("REJECT CASE")), 1000);
//     }
//   });

//   return promise;
// }

// consumer :
// - then().catch()

// function consumePromise() {
//   promiseProducer([2, 2, 3, 4, 5])
//     .then((result) => {
//       console.log("RESPONSE : ", result);
//       return result;
//     })
//     .then((result) => {
//       //   Another Promise
//       promiseProducer([2, 3])
//         .then((resp) => console.log("Second then : ", resp))
//         .catch((err) => {
//           console.error(err);
//         });
//     })
//     .then(() => {
//       console.log("Third then statement");
//     })
//     .catch((err) => {
//       console.error("ERROR -> ", err);
//     });
// }

// consumePromise();

// - Async...await

// async function consumePromise() {
//   try {
//     const response = await promiseProducer([1, 2, 3]);
//     //   Another Promise
//     const result = await promiseProducer(response.message.split(""));
//     console.log("RESULT : ", result);
//   } catch (err) {
//     console.error(err);
//   }
// }

// consumePromise();

// PROMISE API STATIC METHODS

// - resolve : immediately resolve the promise
// - reject : immediately reject the promise
// - all : Array of Promises / either all or nothing
// - allSettled : Array of Promises / successfully settled promise and also the reason for promise failure
// - any : Array of Promises / first successfully settled promise
// - race : Array of Promises / first settled promise, not matter - success of failure

// Promise.resolve({ data: "Hello World" }).then(console.log);

// Promise.reject(new Error("Something went wrong")).catch(console.error);

// const buildPromise = (data, ms) => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       if (ms < 5000) {
//         resolve(data);
//       } else {
//         reject(new Error(data));
//       }
//     }, ms);
//   });
// };

// const buildPromise = (data, ms) => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       if (ms === 1000) {
//         reject(new Error(data));
//       } else {
//         resolve(data);
//       }
//     }, ms);
//   });
// };
// const p1 = buildPromise("Promise One", 3000);
// const p2 = buildPromise("Promise Two", 2000);
// const p3 = buildPromise("Promise Three", 1000);
// const p4 = buildPromise("Promise Four", 6000);

// const promiseArr = [p1, p2, p3, p4];

// Promise.all(promiseArr).then(console.log).catch(console.error);
// Promise.allSettled(promiseArr).then(console.log).catch(console.error);
// Promise.any(promiseArr).then(console.log).catch(console.error);
// Promise.race(promiseArr).then(console.log).catch(console.error);

// FETCH API

window.onload = function () {
  const btnFetch = document.querySelector("#btnFetch");
  const listContainer = document.querySelector("#listContainer");
  btnFetch.addEventListener("click", async function () {
    try {
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/todos"
      );

      const todoCollection = await response.json();

      todoCollection.forEach((todo) => {
        const liElement = document.createElement("li");
        liElement.innerHTML = todo.title;
        listContainer.appendChild(liElement);
      });
    } catch (err) {
      console.log(err);
    }
  });
};

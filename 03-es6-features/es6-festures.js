// DESTRUCTURING : "UNPACKING THE COLLECTION"
// let friends = ["Monica", "Ross", "Joey", "Rachel"];

// let [, , f3, f4, f5] = friends;

// console.log(f4);

// let a = 10,
//   b = 20;

// [b, a] = [a, b];

// console.log(a, b); // ?

// let users = [
//   { email: "john@test", age: 32 },
//   { email: "jenny@test", age: 33 },
//   { email: "james@test", age: 34 },
// ];

// let [o1, o2, o3] = users;

// o1.email = "test@test";

// console.log(users[0]); // ?

// let [
//     { email: email1, age: age1 },
//     { email: email2, age: age2 },
//     { email: email3, age: age3 }
// ] = users

// let userOne = {
//   firstName: "John",
//   lastName: "Doe",
//   age: 32,
//   email: "john@test.com",
//   address: {
//     city: "Pune",
//     street: "201 Main Road, Wakad",
//   },
//   friends: ["Monica", "Ross"],
// };

// let {
//   firstName,
//   address: { city, street },
//   friends: [f1, f2],
// } = userOne;

// console.log(firstName, city, street, f1, f2);

// let userTwo = {
//   firstName: "Jenny",
//   lastName: "Alice",
// };

// let { firstName: firstNameOne, lastName: lastNameOne, email, age } = userOne;

// let { firstName: firstNameTwo, lastName: lastNameTwo } = userTwo;

// console.log(lastNameOne, age);

// ARROW FUNCTION
// Short and clean syntax
// (argsList) => {function_body}
// - 'this' keyword - lexical scope
// - 'arguments' keyword
// - 'new' operator

// const Person = (name) => {
//   // let this = {}
//   this.name = name;
//   // return this;
// };

// let john = new Person("John");

// console.log(john);

// const demoArgs = () => {
//   console.log(arguments);
// };

// demoArgs("test@test", 32, true, {});

// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   getDetails: function () {
//     return () => this.firstName + " " + this.lastName;
//   },
// };

// console.log(user.getDetails()());

// function sum(n1, n2) {
//     return n1 + n2;
// }

// const sum = (n1, n2) => {
//     return n1 + n2
// }

// const sum = (n1, n2) => n1 + n2;

// console.log("Sum : ", sum(2, 4));

// REST / SPREAD OPERATOR (...)
// Rest Operator : creates the collection
// last parameter in function args list

// function demoRest(label, ...args) {
//   console.log(args[0]);
// }

// // demoRest("test@test")
// // demoRest("test@test", 32)
// demoRest("test@test", 32, true);

// Spread : expand the collection

// let marks = [99, 98, 96];

// let cumulateMarks = [92, 94, ...marks];

// console.log(cumulateMarks);

// let userOne = {
//   email: "test@test",
//   company: "Synechron",
// };

// let userTwo = {
//   ...userOne,
//   email: "john@test",
// };

// // console.log(userTwo);

// const targetObject = Object.assign({}, userOne, { email: "james@test" });

// console.log(targetObject);

// TEMPLATE LITERALS

// function demoTagFunc(strings, ...literals) {
//   console.log(strings, literals);
// }

// const varNumbers = 3;
// const varString = "Tagged Template Literals";

// demoTagFunc`This function will contain ${varNumbers} for giving the demonstration of ${varString}`;

// MAP : stores data in the form of keys/values
// - Common built-in functionality
// - Iterable
// - Objects as key

// let user = {
//   name: "John",
// };
// const map = new Map();

// map.set("isAdmin", true);
// map.set("username", "John Doe");
// map.set("age", 32);
// map.set(user, "Doe");

// if (map.has("isAdmin")) {
//   map.delete("isAdmin");
// }

// console.log("Size : ", map.size);

// for (let key of map.keys()) {
//   console.log(key);
// }

// for (let value of map.values()) {
//   console.log(value);
// }

// let test = {};
// for (let [key, value] of map) {
//   test[key] = value;
// }
// console.log(test);

// let obj = {
//     "username": "John"
// };

// console.log(delete obj.notExist);

// Set
// storing unique values

// let set = new Set();

// set.add("john");
// set.add("jenny");
// set.add("james");
// set.add("jack");
// set.add("jill");

// set.add("jill");

// console.log(set.size);

// for (let val of set) {
//   console.log(val);
// }

// WeakMap / WeakSet
// - Removes the values from the collection, when the reference key is NOT available in memory

// let user = { name: "John" };

// const map = new WeakMap();

// map.set(user, "Doe");

// console.log(map.get(user));

// user = null;

// console.log(map.has(user));

// for (let val of map.) {
//   console.log(val);
// }

// DEFAULT PARAMTER

function demoParameter(arr = []) {
  //   arr = arr || [];
  if (arr.length > 2) {
    console.log("Greater than two");
  } else {
    console.log("Less than two");
  }
}

demoParameter([2, 3, 4]);

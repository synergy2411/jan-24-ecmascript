// class Person {
//   constructor(name) {
//     this.name = name;
//   }
//   // Private Method
//   #getName() {
//     return this.name;
//   }
//   getDetails() {
//     return this.#getName();
//   }
// }

// class Student extends Person {
//   //   Private Properties
//   #studId;
//   #courseDuration;

//   constructor(id, name, course) {
//     super(name);
//     this.#studId = id;
//     this.course = course;
//   }

//   // Accessor / Mutator
//   get courseDuration() {
//     return this.#courseDuration;
//   }

//   set courseDuration(value) {
//     if (value.trim() === "") {
//       return;
//     }
//     this.#courseDuration = value;
//   }

//   getDetails() {
//     return `Student Id : ${this.#studId}
//         ${super.getDetails()} is registered with Course ${this.course}
//         `;
//   }
// }

// let john = new Student("S001", "John Doe", "JavaScript");

// console.log(john.getDetails());
// // console.log(john.#studId);           // ERROR - Private Member

// john.courseDuration = "40Hrs";
// console.log(john.courseDuration);

// Protected Member
// class Animal {
//   constructor(species) {
//     if (this.constructor === Animal) {
//       throw new Error("Abstract Class - Can't instantiate");
//     }
//     this.species = species;
//   }
//   getSpecies() {
//     return this.species;
//   }
// }

// class Bird extends Animal {
//   constructor(species, type) {
//     super(species);
//     this.type = type;
//   }
//   fly() {
//     return `${super.getSpecies()} ${this.type} is flying`;
//   }
// }

// let sparrow = new Bird("Bird", "Sparrow");

// console.log(sparrow.fly());

class Student {
  static #numOfStudents = 0;
  constructor(studId, name) {
    this.studId = studId;
    this.name = name;
    Student.#numOfStudents += 1;
  }

  static getTotalStudent() {
    return Student.#numOfStudents;
  }
}

let john = new Student("S001", "John Doe");
let jeeny = new Student("S002", "Jenny");

console.log(Student.getTotalStudent());

console.log(Student.numOfStudents); // PRIVATE STATIC MEMBER

window.onload = function () {
  const labelEl = document.getElementById("txtLabel");
  const btnAdd = document.getElementById("btnAdd");
  const listContainer = document.getElementById("todoListContainer");

  function onDeleteItem(id) {
    const itemToDelete = document.getElementById(id);
    listContainer.removeChild(itemToDelete);
  }

  function addItemToContainer(todo) {
    const liElement = document.createElement("li");
    liElement.innerHTML = `
            <p>${todo.label.toUpperCase()}</p>
        `;
    liElement.setAttribute("id", todo.id);
    liElement.onclick = () => onDeleteItem(todo.id);
    listContainer.appendChild(liElement);
  }

  btnAdd.addEventListener("click", function (event) {
    event.preventDefault();
    const label = labelEl.value;
    if (label.trim() === "") {
      return;
    }
    const todo = {
      id: Math.round(Math.random() * 100),
      label,
    };
    addItemToContainer(todo);
    labelEl.value = "";
  });
};

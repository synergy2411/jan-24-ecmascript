// function createIterator(start, end, step) {
//   let counter = start;
//   const range = {
//     next() {
//       let result;
//       if (counter < end) {
//         result = { value: counter, done: false };
//         counter += step;
//         return result;
//       }
//       return { value: "Completed", done: true };
//     },
//   };

//   return range;
// }

// const rangeIterator = createIterator(1, 10, 2);

// console.log(rangeIterator.next());
// console.log(rangeIterator.next());
// console.log(rangeIterator.next());
// console.log(rangeIterator.next());
// console.log(rangeIterator.next());
// console.log(rangeIterator.next());

// const arr = ["A", "B", "C"];

// const it = arr[Symbol.iterator]();

// console.log(it.next());
// console.log(it.next());
// console.log(it.next());

// console.log(it.next());

// const arr = ["A", "B", "C"];

// function* anotherGenerator() {
//   yield "do";
//   yield "Something";
// }

// function* theGenerator() {
//   yield 101;
//   yield 201;
//   yield* arr;
//   yield* anotherGenerator();
// }

// const it = theGenerator();
// console.log(it.next());
// console.log(it.next());

// for (let val of theGenerator()) {
//   console.log(val);
// }

// async function* fetchData() {
//   try {
//     const responseOne = await fetch("http://localhost:3000/payments");
//     const resultOne = await responseOne.json();
//     yield resultOne;

//     const responseTwo = await fetch(
//       "https://jsonplaceholder.typicode.com/posts"
//     );
//     const resultTwo = await responseTwo.json();
//     yield resultTwo;
//   } catch (err) {
//     console.error(err);
//   }
// }

// async function consumeData() {
//   try {
//     const it = fetchData();
//     const resultOne = await it.next();
//     console.log(resultOne);
//     const resultTwo = await it.next();
//     console.log(resultTwo);
//   } catch (err) {
//     console.log(err);
//   }
// }

// consumeData();

// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   [Symbol.iterator]() {
//     const it = {
//       next() {
//         return { value: "XYZ", done: false };
//       },
//     };
//     return it;
//   },
// };

// const it = user[Symbol.iterator]();

// console.log(it.next());

// for (let val of user) {
//   //   console.log(val);
// }

// Promise.resolve("Data Arrived")
//   .then(console.log)
//   .finally(() => {
//     console.log("FINALLY");
//   });

// Promise.reject(new Error("Something went wrond"))
//   .then(console.log)
//   .catch(console.error)
//   .finally(() => {
//     console.log("FINALLY");
//   });

// let user = {
//   name: "John Doe",
// };

// Object.defineProperty(user, "name", {
//   writable: false,
// });

// user.name = "Jenny";

// console.log(user.name);

// const description = Object.getOwnPropertyDescriptor(user, "name");

// console.log(description);

// let userTwo = Object.freeze({
//   name: "James",
//   age: 32,
// });

// userTwo.name = "Jill";

// userTwo.isAdmin = true;

// delete userTwo.name;

// userTwo = null;

// console.log(userTwo);

let str = "Hello World";
const newStr = str.padEnd(20, "Z");

console.log(newStr);

let creditCardNumber = "2424412398765678";
const fourDigits = creditCardNumber.slice(-4);

const maskedCardNumber = fourDigits.padStart(16, "*");

console.log(maskedCardNumber);

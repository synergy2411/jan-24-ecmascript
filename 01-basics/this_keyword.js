// function demoThis() {
//   console.log(this);
// }

// demoThis();

// alert("Who's this?");

// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   getDetails: function () {
//     console.log(this.firstName, this.lastName);
//   },
// };

// user.getDetails();

// ARROW FUNCTION
// let user = {
//   firstName: "John",
//   lastName: "Doe",
//   getDetails: function () {
//     let nestedFn = () => this.firstName + " " + this.lastName;
//     return nestedFn;

//     // let self = this;
//     // let nestedFn = function () {
//     //   return self.firstName + " " + self.lastName;
//     // };
//     // return nestedFn;
//   },
// };

// const nestedFn = user.getDetails();

// console.log(nestedFn());

// EVENT LISTENER

// const txtInputEl = document.querySelector("#txtInput");
// const btnDemo = document.querySelector("#btnDemo");

// btnDemo.addEventListener("click", function () {
//   console.log(this);
// });

// txtInputEl.addEventListener("input", function () {
//   console.log(this); // ?
// });

// CALL / APPLY / BIND

// function greet() {
//   console.log("Hello ", this.firstName);
// }

// let userOne = {
//   firstName: "John Doe",
// };
// let userTwo = {
//   firstName: "Jenny",
// };

// const boundedFn = greet.bind(userOne);
// const boundedFn2 = greet.bind(userTwo);

// boundedFn();

// boundedFn2();

function greet(language, message) {
  if (language === "de") {
    console.log("Hola " + message + " " + this.firstName);
  } else {
    console.log("Hello " + message + " " + this.firstName);
  }
}

let userOne = {
  firstName: "James",
};

greet.call(userOne, "de", "How are you?");
greet.apply(userOne, ["en", "How are you?"]);

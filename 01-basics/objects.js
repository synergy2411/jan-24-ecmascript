// Object : unordered collections
// Literal Method

// const books = ["book1", "book2", "book3"];

// let box = {
//   width: 4,
//   height: 6,
//   books: books,
//   "# of books": 3,
//   addBook: function (book) {
//     this.books.push(book);
//   },
// };

// box.volume = box.width * box.height;

// box.addBook("book4");

// console.log(box.books.length); //

// console.log(books.length); // 4

// console.log(delete box.widht);

// console.log(box);
// console.log(box.books);

// console.log(box["# of books"]);

// Constructor Method
// - to create similar types of objects
// - to greed Java Developers

// function Person(firstName, lastName) {
// var this = {};
// this.firstName = firstName;
// this.lastName = lastName;

// this.getDetails = function () {
//   return this.firstName + " " + this.lastName;
// };
// return this;
// }

// Person.prototype.getDetails = function () {
//   return "Hello " + this.firstName + " " + this.lastName;
// };

// let john = new Person("John", "Doe");
// console.log(john.getDetails());

// let jenny = new Person("Jenny", "Doe");
// console.log(jenny.firstName);

// console.log(jenny.constructor);

// console.log(Person.isPrototypeOf(john));

// console.log(john.getDetails());

// let str = "Hello World";
// let str = new String("Hello World")

// console.log(str.length);

// console.log(str.toUpperCase());

// Instance Method
// - Inheritance - Prototype based

let shoe = {
  size: 8,
};

let magicShoe = Object.create(shoe);

magicShoe.type = "Sneaker";

// console.log(shoe.isPrototypeOf(magicShoe));

// console.log(Object.prototype.isPrototypeOf(shoe)); // ?

// console.log(Object.prototype.isPrototypeOf(magicShoe)); // ?

// console.log(magicShoe.size);
// console.log(magicShoe.hasOwnProperty("size"));
// console.log(magicShoe.hasOwnProperty("type"));
// console.log(magicShoe.hasOwnProperty("xyz"));

// console.log(magicShoe.size);
// magicShoe.size = 10;
// magicShoe.type = "Sneaker";

console.log(magicShoe);

// console.log(shoe.size); // ?

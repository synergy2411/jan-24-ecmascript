// function longRunning() {
//   setTimeout(function () {
//     console.log("Running the operation");
//   }, 1500);
// }

// function webRequest() {
//   console.log("Starting Request");
//   longRunning();
//   console.log("Ending Request");
// }

// webRequest();
// webRequest();

// Starting
// Ending

// Starting
// Ending
// After 2 seconds
// Running
// Running

// function demoAsync() {
//   console.log("Start");

//   setTimeout(function () {
//     console.log("Timer");
//   }, 0);

//   Promise.resolve("Promise API").then(console.log);

//   console.log("End");
// }

// demoAsync();

// Start
// End
// Promise API
// Timer

// var x = 10;
// console.log(typeof x);

// x = "Hello";
// console.log(typeof x); // ?

// var x = 10;

// var y = "20";

// let result = x + y;

// let result = y - x;

// console.log(result); // 10

// let & const vs var
// - restricts the scope to the block

// function demoBlock(arr) {
//   if (arr.length > 2) {
//     let load = "LOADING";
//     console.log(flash); // ?
//   } else {
//     let flash = "FLASHING";
//   }
// }

// demoBlock([2, 3, 4, 5]);

// const USERNAME = "John Doe";

// USERNAME = "Jenny";

// console.log(USERNAME);

// const user = {
//   // xix0001
//   name: "John Doe",
// };

// user.name = "Jenny";

// console.log(user.name); // ?

// user = {
//   name: "James",
// };

// const friends = ["Monica", "Ross"];

// friends.push("Joey");

// console.log(friends); // ?

// friends = ["Monica", "Ross", "Joey"]; // ERROR

// function demoVar() {
//   var x = 101;
// }

// console.log(x);

// console.log(30 > 20 > 10); // ?

// BACK TICK (``)
// - Embed variables withing string without (+)
// - Multiline string without (\n)

// let username = "John Doe";
// let age = 32;

// let greeting = `Hello ${username},
// I'm ${age} years old!
// `;

// console.log(greeting);

// let x = "Hello";

// let y = parseInt(x);

// let z = Number(x);

// console.log(y, z);

// console.log(typeof y);
// console.log(typeof z);

// let friends = ["Monica", "Joey", "Rachel", "Ross"];

// for (let i = 0; i < friends.length; i++) {
//   console.log(friends[i]);
// }

// for...of (Iterators / generators)
// for (let friend of friends) {
//   console.log(friend);
// }

// let user = {
//   email: "test@test",
//   age: 32,
//   isAdmin: true,
// };

// for (let item of user) {
//   console.log(item);
// }

// for (let key in user) {
//   console.log(user[key]);
// }

// for (let [key, value] of Object.entries(user)) {
//   console.log(`${key} : ${value}`);
// }

// for (let key of Object.keys(user)) {
//   console.log(key);
// }
// for (let value of Object.values(user)) {
//   console.log(value);
// }

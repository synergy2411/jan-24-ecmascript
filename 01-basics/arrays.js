// let arr = ["Monica", "Joey", "Ross"];

// let arr2 = [
//   "Rachel",
//   32,
//   true,
//   function () {
//     console.log("Hello there");
//   },
// ];

// arr2[3]();

// let arr = new Array(99, 98, 97);

// let arr2 = new Array(99);

// console.log(arr.length);
// console.log(arr2.length);

// let numbers = [99, 98, 96, 92];

// console.log(numbers["1"]);

// SHALLOW COPY
// - References will be supplied to the new object

// let user = {
//   firstName: "John",
//   friends: ["Monica", "Joey"],
//   addFriend: function (friend) {
//     this.friends.push(friend);
//   },
//   dob: new Date("Dec 1, 1976"),
// };

// let userTwo = { ...user };

// console.log(userTwo);

// userTwo.firstName = "Jenny";

// console.log(user.firstName); // ?

// userTwo.friends[0] = "James"; // ?

// console.log(user.friends);

// DEEP COPY

// let userTwo = JSON.parse(JSON.stringify(user));

// userTwo.friends[0] = "Jack";

// userTwo.addFriend("Jill");

// console.log(user.friends.length);   // ?

// console.log(JSON.parse(JSON.stringify(user)));

// let numbers = [99, 97, 96, 92, 89];

// numbers.shift();

// numbers.unshift(100);

// numbers.push(101);

// numbers.pop();

// numbers.fill(0, 2, 4);

// numbers.sort(function (a, b) {
//   if (a > b) {
//     return -1;
//   } else if (b > a) {
//     return +1;
//   } else {
//     return 0;
//   }
// });

// let deletedItems = numbers.splice(0, 2);
// console.log(deletedItems);

// numbers.reverse();

// const mappedArray = numbers.map(function (value, index, array) {
//   return value + 1;
// });

// console.log(mappedArray);

// let filteredArray = numbers.filter(function (value) {
//   return value > 96;
// });
// console.log(filteredArray);

// let slicedArray = numbers.slice(1, 3);
// console.log(slicedArray);

// let concatedArray = numbers.concat(101);
// console.log(concatedArray);

// let position = numbers.indexOf(96);
// console.log(position);

// let position = numbers.findIndex(function (value) {
//   return value > 92;
// });

// console.log(position);

// let el = numbers.find(function (value) {
//   return value < 95;
// });

// console.log(el);

// const isIncluded = numbers.includes(101);

// console.log(isIncluded);

// let isAvailable = numbers.some(function (value) {
//   return value > 100;
// });

// console.log(isAvailable);

// let isAvailable = numbers.every(function (value) {
//   return value > 80;
// });

// console.log(isAvailable);

// let joinedArray = numbers.join(" ");
// console.log(joinedArray);

// let reducedValue = numbers.reduce(function (prevValue, currValue) {
//   return (prevValue += currValue);
// }, 100);

// console.log(reducedValue);

// let forEachValue = numbers.forEach(function (value) {
//   console.log(value);
// });

// console.log(forEachValue);

// console.log(numbers);

// let users = [
//   { id: "u001", email: "john@test", age: 32 },
//   { id: "u002", email: "jenny@test", age: 34 },
//   { id: "u003", email: "james@test", age: 23 },
//   { id: "u004", email: "jack@test", age: 28 },
// ];

// let position = users.findIndex((user) => user.age === 23);

// users.splice(position, 1);

// console.log(users);

// let totAge = users.reduce(function (prev, curr) {
//   return (prev += curr.age);
// }, 0);

// let avgAge = totAge / users.length;

// console.log(avgAge);

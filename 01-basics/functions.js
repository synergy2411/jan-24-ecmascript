// demoFuncDeclaration();

// function demoFuncDeclaration() {
//   console.log("Function declaration Called");
// }

// function mystry() {
//   var chooseNumber = function () {
//     return 7;
//   };

//   return chooseNumber;

//   var chooseNumber = function () {
//     return 12;
//   };
// }

// const nestedFn = mystry(); // ?

// console.log(nestedFn()); // 12

// CALLBACKS -> error first, callback last

// function greet(err, name) {
//   if (err) {
//     console.error(err);
//     return;
//   }
//   console.log("Guten Morgen " + name);
// }

// // HOF
// function display(arr, cb) {
//   if (arr.length > 2) {
//     cb(null, "John Doe");
//   } else {
//     cb(new Error("Something went wrong"));
//   }
// }

// display([1, 2], greet);

// setTimeout(function () { }, 1500);

// function mul(n1, n2, n3) {
//   return n1 * n2 * n3;
// }

// console.log(mul(5, 7, 2));

// Partials - Currying
// function a(n1) {
//   return function b(n2) {
//     return function c(n3) {
//       return n1 * n2 * n3;
//     };
//   };
// }

// let multiplyBy5 = a(5);

// let multiplyBy15 = multiplyBy5(7);

// let result = multiplyBy15(2);

// console.log(result);

// (a,b,c) => (a)=>(b)=>(c)

// CLOSURES - process of binding the outer scope variables to the nested function
// function testClosure() {
//   let x = 4;
//   return function () {
//     return ++x;
//   };
// }

// let nestedFn = testClosure();

// console.log(nestedFn()); // 5
// console.log(nestedFn()); // 6
// console.log(nestedFn()); // 7
// console.log(nestedFn()); // 8

// function buildTicket(transport) {
//   let numOfPassenger = 0;
//   return function (name) {
//     return `Hello ${name}!
//     You are going via ${transport}!
//     Your ticket id # ${++numOfPassenger}`;
//   };
// }

// const shipFn = buildTicket("Ship");

// console.log(shipFn("John"));
// console.log(shipFn("Jenny"));

// const kangaroo = buildTicket("Kangaroo");
// console.log(kangaroo("Jack")); // ?

// Scope Chaining - Variable lookup

// let x = 301;
// function a() {
//   // let x = 101;
//   function b() {
//     // let x = 201;
//     console.log(x);
//   }
//   b();
// }
// a();

// Lexical Scope / Environment
// Physical location of function

// let x = 201;

// function b() {
//   console.log(x); // 201
// }

// function a() {
//   let x = 101;
//   b();
// }

// a();

// (function () {
//   var x = 101;
//   console.log("IIFE Works", x);
// })();

// console.log(x);
